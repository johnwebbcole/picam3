// title      : picam3
// author     : John Cole
// license    : ISC License
// file       : picam3.jscad

/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  /**
   * Add the keys to the `Parts` object
   * here and they will be listed in the
   * parameters.
   */
  var ENABLED = {
    cameraCaseTop: true,
    cameraCaseBottom: true,
    piCaseTop: true,
    piCaseBottom: true,
    buttons: true,
    displayPad: true,
    camera: true,
    rpi: true,
  };

  return [
    { type: 'group', name: 'Parts' },
    ...Object.keys(ENABLED).map((name) => {
      return {
        name,
        type: 'checkbox',
        checked: ENABLED[name],
      };
    }),
    {
      name: 'resolution',
      type: 'choice',
      values: [0, 1, 2, 3, 4],
      captions: [
        'low (8,24)',
        'normal (12,32)',
        'high (24,64)',
        'very high (48,128)',
        'ultra high (96,256)',
      ],
      default: 0,
      initial: 0,
      caption: 'Resolution:',
    },
    { type: 'group', name: 'View' },
    {
      name: 'center',
      type: 'checkbox',
      checked: false,
    },
    { type: 'group', name: 'Cutaway' },
    {
      name: 'cutawayEnable',
      caption: 'Enable:',
      type: 'checkbox',
      checked: false,
    },
    {
      name: 'cutawayAxis',
      type: 'choice',
      values: ['x', 'y', 'z'],
      initial: 'y',
      caption: 'Axis:',
    },
  ];
}

function main(params) {
  var start = performance.now();
  var resolutions = [
    [6, 16],
    [8, 24],
    [12, 32],
    [24, 64],
    [48, 128],
  ];
  var [defaultResolution3D, defaultResolution2D] = resolutions[
    parseInt(params.resolution)
  ];
  CSG.defaultResolution3D = defaultResolution3D;
  CSG.defaultResolution2D = defaultResolution2D;
  util.init(CSG /* , { debug: '*' } */);

  // var b = Parts.Cube([50, 75, 25]).Center();
  // var e = b.enlarge(10, 10, 10);
  // var bb = Boxes.Rabett(e.subtract(b), 5, 0.04, 20, 4);
  // console.log(bb);
  // return [bb.combine('top').translate([0, 0, 25]), bb.combine('bottom')];

  var caseThickness = 0.35 * 9;
  var topBottomThickness = 0.15 * 15;
  var piCase = PiCase(caseThickness, topBottomThickness);
  var cameraCase = CameraCase(caseThickness)
    .rotate('camera-board', 'z', 90)
    .rotate('camera-board', 'y', 180)
    .snap('bottom', piCase.parts.bottom, 'z', 'outside+')
    .snap('bottom', piCase.parts.bottom, 'y', 'inside+')
    .align('camera-sensor', piCase.combine('pi-gpio'), 'x');

  var partList = {
    cameraCaseTop: () => {
      return cameraCase
        .combine('top')
        .color('gray')
        .subtract([
          ...cameraCase.array(
            'camera-hole1,camera-hole2,camera-hole3,camera-hole4'
          ),
          ...cameraCase.array(
            'camera-mount-clearance,camera-tripodMount',
            (p) => p.enlarge([0.5, 0.5, 0.5])
          ),
        ]);
    },

    cameraCaseBottom: () => {
      return cameraCase
        .combine('bottom,post1,post2,post3,post4')
        .color('gray')
        .subtract(
          cameraCase.array('camera-tripodMount,ribbon-hole', (p) =>
            p.enlarge([0.25, 0.25, 0.25])
          )
        );
    },

    piCaseHoles: (piCase, cameraCase) => {
      return [
        piCase.parts['pi-clearance-usbc'].fit(12, 0, 7.5),
        piCase.parts['pi-clearance-hdmi1'].fit(12, 0, 7.5),
        // piCase.parts['pi-clearance-hdmi2'].fit(12, 0, 7.5),
        // piCase.parts['pi-clearance-avjack'].enlarge([1, 0, 1]),
        ...piCase.holes,
        // ...piCase.array('pi-clearance-usbc', (p) => p.enlarge([1, 0, 0])),
        // ...piCase.array('pi-ethernet', (p) => p.enlarge([2, 1, 0])),
        // ...piCase.array(
        //   'pi-usb1body,pi-usb2body,pi-usb1flange,pi-usb2flange',
        //   (p) => p.enlarge([0, 0, 0]).translate([0, 0, 1])
        // ),
        ...piCase.array('pi-clearance-usb2,pi-clearance-usb1', (p) =>
          p.enlarge([2, 0, 0]).translate([-1, 0, 1])
        ),
        ...cameraCase.array(
          'camera-hole1,camera-hole2,camera-hole3,camera-hole4,ribbon-hole',
          (h) => h.snap(piCase.parts.bottom, 'z', 'inside-')
        ),
        piCase
          .combine('pi-microsd')
          .enlarge([5, 0.5, 2])
          .snap(piCase.parts['pi-microsd'], 'z', 'inside+', 0.5),
        piCase.combine('pi-ethernet').enlarge([2, 1, 0]),
        piCase.combine('pi-clearance-ethernet').enlarge([1, 0, 0]),
      ];
    },
    piCaseTop: () => {
      return piCase
        .combine('top,topPad1,topPad2,topPad4,topPad3,screen-pad')
        .color('darkgray')
        .subtract([
          piCase.holes,
          ...piCase.array('pi-usb1body,pi-usb2body,pi-ethernet', (p) =>
            p.enlarge([0, 0.5, 0.5])
          ),
          ...piCase.array(
            'pi-usb1flange,pi-usb2flange,pi-miniPiTFT-display-clearance,pi-miniPiTFT-button-connector-clearance,pi-miniPiTFT-buttonCap1-clearance,pi-miniPiTFT-buttonCap2-clearance'
          ),
          ...partList.piCaseHoles(piCase, cameraCase),
        ]);
      // .translate([0, 0, 5])
      // .subtract([...piCase.array('pi-clearance-display')]);
    },

    piCaseBottom: () => {
      console.log('piCase', piCase);
      return piCase
        .combine('bottom', { noholes: true })
        .union(
          piCase.array('pi-pad1,pi-pad2,pi-pad3,pi-pad4', (pad) =>
            pad.fillet(-2, 'z-')
          )
        )
        .color('gray')

        .subtract(partList.piCaseHoles(piCase, cameraCase));
    },

    buttons: () => {
      return piCase.combine(
        'pi-miniPiTFT-buttonCap1,pi-miniPiTFT-buttonCap2,pi-miniPiTFT-button-connector'
      );
    },

    displayPad: () => {
      return piCase.combine('pi-miniPiTFT-standoff');
    },

    camera: () => {
      return cameraCase.combine('camera');
    },

    rpi: () => {
      console.log('piCase', piCase);
      return piCase.combine('pi,pi-miniPiTFT');
    },

    assembled: () => {
      return [
        // piCase.combine('pi'),
        // cameraCase.combine('camera'),

        // parts.piCaseBottom(),
        parts.cameraCaseBottom(),
        // parts.piCaseTop().translate([0, 0, 15]),
        parts.cameraCaseTop(),
        // cameraCase.combine('ribbon-hole', { noholes: true }),
      ];
    },
  };

  var selectedParts = Object.entries(partList)
    .filter(([key, value]) => {
      return params[key];
    })
    .reduce((parts, [key, value]) => {
      var part = value();
      if (Array.isArray(part)) parts = parts.concat(part);
      else parts.push(part);
      return parts;
    }, []);

  console.log('selectedParts', selectedParts);
  var parts = selectedParts.length ? selectedParts : [util.unitAxis(20)];

  if (params.center) parts = [union(parts).Center()];
  if (params.cutawayEnable)
    parts = [util.bisect(union(parts), params.cutawayAxis).parts.positive];

  console.log('timer', performance.now() - start);

  /**
   * If you are using @jwc/jscad-hardware and want
   * a Bill Of Materials (BOM), uncomment this and
   * open the DevTools console to see the BOM
   * list.
   */
  // console.log(
  //   'BOM\n',
  //   Object.entries(Hardware.BOM)
  //     .map(([key, value]) => `${key}: ${value}`)
  //     .join('\n')
  // );

  console.log('parts', ...parts);

  return [...parts];
}

function CameraCase(caseThickness) {
  var camera = RaspberryPi.HQCameraModule().rotate('sensor', 'z', 90);
  var c = camera.combine('board,ribbon,ribbon-nogo');
  var bbox = Parts.BBox(c)
    .color('blue')
    .stretch('z', 5)
    .snap(c, 'z', 'inside+')
    .align(c, 'xy')
    .enlarge([1, 1, 1]);

  var shell = bbox.enlarge([caseThickness, caseThickness, caseThickness]);
  var shellSize = shell.size();
  var shell2 = Parts.RoundedCube(
    shellSize.x,
    shellSize.y,
    shellSize.z,
    2
  ).align(shell, 'xyz');
  var box = Boxes.Rabett(
    shell2.subtract(bbox),
    caseThickness / 2,
    0.2,
    -caseThickness / 2,
    1
  );
  console.log(
    'box',
    util.calcSnap(box.parts.bottom, camera.parts.board, 'z', 'inside-'),
    { caseThickness }
  );

  var post = Parts.Cylinder(
    5,
    util.calcSnap(box.parts.bottom, camera.parts.board, 'z', 'inside-')[2] -
      caseThickness / 2
  )
    .fillet(-2, 'z-')
    .color('gray')
    .snap(box.parts.bottom, 'z', 'inside-', caseThickness / 2)
    .align(camera.parts.hole1, 'xy');
  box.add(post, 'post1');
  box.add(post.align(camera.parts.hole2, 'xy'), 'post2');
  box.add(post.align(camera.parts.hole3, 'xy'), 'post3');
  box.add(post.align(camera.parts.hole4, 'xy'), 'post4');

  box.add(camera, 'camera', true, 'camera-');

  box.add(
    box
      .combine('camera-ribbon-nogo')
      .enlarge([0, 0, 4])
      .snap(box.parts.bottom, 'z', 'inside-'),
    'ribbon-hole',
    true
  );

  box.holes.push(
    ...box.array('camera-hole1,camera-hole2,camera-hole3,camera-hole4', (h) =>
      h.enlarge([Hardware.Clearances.loose, Hardware.Clearances.loose, 20])
    )
  );

  return box;
}

function PiCase(caseThickness, topBottomThickness) {
  var pi = RaspberryPi.BPlus(4, { clearance: caseThickness }).center();

  pi.add(
    RaspberryPi.MiniPiTFT()
      .snap('board', pi.parts.mb, 'x', 'inside-')
      .snap('board', pi.parts.mb, 'y', 'inside+')
      .snap('board', pi.parts.mb, 'z', 'outside-', 7.5),
    'miniPiTFT',
    true,
    'miniPiTFT-'
  );

  // var post = Parts.Cube([4, 4, 15])
  //   .align(pi.holes[0], 'xy')
  //   .snap(pi.parts.mb, 'z', 'outside-')
  //   .color('black');
  // pi.add(post, 'post1');
  // pi.add(post.align(pi.holes[1], 'xy'), 'post2');
  // pi.add(post.align(pi.holes[2], 'xy'), 'post3');
  // pi.add(post.align(pi.holes[3], 'xy'), 'post4');

  // var screen = Parts.Cube([97, 59, 6])
  //   .snap(post, 'z', 'outside-', 2)
  //   .snap(pi.parts.mb, 'x', 'inside+', 1)
  //   .align(pi.parts.mb, 'y')
  //   .color('darkgreen');

  // var display = Parts.Cube([88, 54, 6])
  //   .snap(screen, 'xyz', 'inside+')
  //   .translate([-2.5, 0, 0])
  //   .align(screen, 'y')
  //   .color('black');

  // pi.add(screen.subtract(display).union(display), 'screen');

  // pi.add(
  //   Parts.Cube([88, 54, 2])
  //     .align(display, 'xy')
  //     .snap(display, 'z', 'outside-')
  //     .color('red'),
  //   'clearance-display',
  //   true
  // );

  var pad = Parts.Cylinder(7, 5)
    .snap(pi.parts.mb, 'z', 'outside+')
    .align(pi.holes[0], 'xy')
    .color('blue');

  pi.add(pad, 'pad1');
  pi.add(pad.align(pi.holes[1], 'xy'), 'pad2');
  pi.add(pad.align(pi.holes[2], 'xy'), 'pad3');
  pi.add(pad.align(pi.holes[3], 'xy'), 'pad4');

  var x = pi
    .combine('mb,avjack,miniPiTFT-button1-push,pad1') // microsd
    .union(pi.combine('ethernet,usb1,usb2').bisect('z', 1).parts.negative);
  console.log('pi', pi);

  var bbox = Parts.BBox(x)
    // .enlarge([0, 0, 0])
    .color('white')
    .align(x, 'xyz');

  var bbox = bbox
    .translate([0, 0, 0])
    // .stretch('x', 3, 0)
    .snap(bbox, 'x', 'inside+', -2)
    .stretch('y', 2, 0)
    .snap(bbox, 'y', 'inside-');
  // .bisect('z', 5)
  // .parts.positive
  // .translate([0, 0, -caseThickness]);

  var exterior = bbox.enlarge([
    caseThickness,
    caseThickness,
    topBottomThickness,
  ]);
  exterior = exterior
    // .stretch('z', 2, 0)
    // .snap(exterior, 'z', 'inside+')
    .subtract(bbox);

  var eSize = exterior.size();
  var rexterior = Parts.RoundedCube(eSize.x, eSize.y, eSize.z, 2)
    .align(exterior, 'xyz')
    .subtract(bbox);

  var box = Boxes.Rabett(
    rexterior,

    caseThickness / 2,
    0.3,
    -caseThickness / 2,
    2 // 5
  );

  box.holes
    .push
    // ...pi
    //   .array(
    //     'clearance-display'
    //   )
    //   .map((p) => p.enlarge([0.5, 0.5, 1])),
    // ...pi
    //   .array(
    //     'clearance-ethernet,clearance-usb1' //,clearance-usb2,clearance-avjack
    //   )
    //   .map((p) => p.enlarge([0.5, 0.5, 1])),
    // ...pi.array('clearance-hdmi1,clearance-hdmi2').map((p) => {
    //   return p.enlarge([2, 3, 1]);
    // })
    ();

  box.add(pi, 'pi', true, 'pi-');

  box.holes.push(
    ...pi.array('hole1,hole2,hole3,hole4', (h) =>
      h.enlarge([0, 0, 20]).snap(box.parts.bottom, 'z', 'inside-')
    )
  );

  var top2boardHeight =
    util.calcSnap(box.parts.top, box.parts['pi-mb'], 'z', 'inside+')[2] * -1 -
    caseThickness / 2;

  console.log('*** height', top2boardHeight);

  var topPad = Parts.Cylinder(7, top2boardHeight)
    .snap(box.parts.top, 'z', 'inside+', -(topBottomThickness / 2))
    .align(pi.holes[0], 'xy')
    .fillet(-1, 'z+')
    .color('blue');

  box.add(topPad, 'topPad1', true);
  box.add(topPad.align(pi.holes[1], 'xy'), 'topPad2', true);
  // box.add(topPad.align(pi.holes[2], 'xy'), 'topPad3', true);
  box.add(topPad.align(pi.holes[3], 'xy'), 'topPad4', true);

  var top2tftHeight =
    util.calcSnap(
      box.parts.top,
      box.parts['pi-miniPiTFT-board'],
      'z',
      'inside+'
    )[2] *
      -1 -
    topBottomThickness / 2;

  var topPad3 = Parts.Cylinder(7, top2tftHeight + 0.25)
    .snap(box.parts.top, 'z', 'inside+', -(topBottomThickness / 2))
    .align(pi.holes[2], 'xy')
    .fillet(-0.5, 'z+')
    .color('blue');

  box.add(topPad3, 'topPad3', true);

  var top2displayHeight =
    util.calcSnap(
      box.parts.top,
      box.parts['pi-miniPiTFT-display'],
      'z',
      'inside+'
    )[2] *
      -1 -
    topBottomThickness / 2;
  box.add(
    box.parts['pi-miniPiTFT-screen']
      .fit(0, 0, top2displayHeight)
      .snap(box.parts.top, 'z', 'inside+', -(topBottomThickness / 2)),
    'screen-pad'
  );
  console.log('box', box);
  return box;
}

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
